import {View, Text} from 'react-native';
import React from 'react';
import Route from './src/Route/Route';
import {Provider} from 'react-redux';
import storeRedux from './src/Redux/Store';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <Route />
    </Provider>
  );
};

export default App;