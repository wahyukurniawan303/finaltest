import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  style,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Getone} from '../Redux/Action/Action';
import Regis from './Regis';
import Logo from '../../Assets/Pay.png';
import Logo1 from '../../Assets/login3.png';

const Login = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  async function coba() {
    dispatch(Getone());
    try {
    } catch (e) {
      console.log(e, 'error login');
    }
  }

  // useEffect(() => {
  //   setTimeout(() => {
  //     coba();
  //   }, 2000);
  // }, []);
  const state = useSelector(state => state.auth);
  console.log(state.AuthData, 'ini state');
  const MultilineTextInputExample = () => {
    const [value, onChangeText] = React.useState(
      'Useless Multiline Placeholder',
    );
  };

  return (
    <View style={[styles.container]}>
      {/* <View style={{flex: 1, backgroundColor: 'red'}} /> */}
      <View style={{alignSelf: 'center'}}>
        <Image source={Logo1} style={styles.image} />
      </View>
      <View style={styles.top1}>
        <View style={styles.top2}>
          <Text style={styles.text}>Hi Welcome!</Text>
          <Text style={styles.text2}>Sign in to Transaction</Text>
        </View>
        <View style={styles.top3}>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn3.iconfinder.com/data/icons/iconoid-ui-essentials/32/ui_user_account_person_username-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              style={styles.username}
              placeholder="Username"
              value={username}
              onChangeText={setUsername}
              editable
              multiline
              numberOfLines={1}
              maxLength={70}
            />
          </View>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn0.iconfinder.com/data/icons/essentials-4/1710/lock-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              style={styles.pass}
              placeholder="Password"
              secureTextEntry
              value={password}
              onChangeText={setPassword}
              editable
              multiline
              numberOfLines={1}
              maxLength={100}
            />
          </View>
        </View>
      </View>
      <View>
        <TouchableOpacity
          style={styles.login}
          title="Sign In"
          onPress={() => {
            Alert.alert('Welcome Back !!! ');
            coba();
          }}>
          <Text style={styles.textlogin}>Sign In</Text>
        </TouchableOpacity>
      </View>
      {/* <View style={{paddingTop: 10, paddingHorizontal: 160}}>
        <Button
          title="LOGIN"
          onPress={() => {
            coba();
          }}
        />
      </View> */}
      <View style={styles.regis}>
        <Text style={styles.text3}>Don't have an account ?</Text>
        <View style={{marginLeft: 5}}>
          <TouchableOpacity
            style={styles.register}
            title="Sign Up"
            onPress={() => {
              Alert.alert('Silahkan Isi Data Diri ');
              navigation.navigate('Regis');
            }}>
            <Text style={{textAlign: 'center', fontFamily: 'Montserrat-Bold'}}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      {/* <View style={{paddingTop: 10, paddingHorizontal: 160}}>
        <Button
          title="Register"
          onPress={() => {
            navigation.navigate('Regis');
          }}
        />
      </View> */}

      <View style={{paddingTop: 10}}>
        {/* <Button title="Logout" onPress={''} /> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'darkturquoise',
  },
  top1: {
    alignItems: 'center',
    flexDirection: 'column',
  },
  top2: {
    alignSelf: 'flex-start',
    paddingHorizontal: 35,
    // paddingVertical: 10,
  },
  top3: {
    alignSelf: 'center',
    paddingHorizontal: 35,
    paddingVertical: 10,
  },
  top4: {
    flexDirection: 'row',
    borderBottomWidth: 1,
  },
  icon: {
    height: 30,
    width: 30,
    marginTop: 20,
  },
  username: {
    fontSize: 20,
    // backgroundColor: '#FFFACD',
    width: 200,
    // marginVertical: 10,
    padding: 15,
    borderBottomColor: 'black',
    // marginTop: 10,
    // borderRadius: 15,
    // borderWidth: 2,
    // borderColor: '#6495ED',
  },
  pass: {
    fontSize: 20,
    // backgroundColor: '#FFFACD',
    width: 300,
    padding: 15,
    borderBottomColor: 'black',
    // marginTop: 10,
    // borderBottomWidth: 1,
    // borderRadius: 15,
    // borderWidth: 2,
    // borderColor: '#6495ED',
  },
  login: {
    fontSize: 50,
    backgroundColor: 'honeydew',
    marginTop: 20,
    width: 150,
    padding: 10,
    borderRadius: 15,
    alignSelf: 'center',
  },
  regis: {
    alignSelf: 'center',
    paddingHorizontal: 40,
    paddingVertical: 20,
    flexDirection: 'row',
    marginTop: 35,
  },
  image: {
    // flex: 1,
    height: 210,
    width: 900,
    resizeMode: 'contain',
    backgroundColor: 'darkturquoise',
    // alignItems: 'stretch',
    marginTop: 10,
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 28,
    color: 'black',
  },
  text2: {
    fontFamily: 'Montserrat-Light',
    fontSize: 20,
    color: 'black',
  },
  text3: {
    fontFamily: 'Montserrat-Light',
    fontSize: 15,
    color: 'black',
    alignItems: 'center',
  },
  textlogin: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
  },
});

export default Login;