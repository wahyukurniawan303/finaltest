import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Button,
  StyleSheet,
  Image,
  Alert
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {SignUp} from '../Redux/Action/Action';
import Logo2 from '../../Assets/regis2.png';
// const TextInputLabel = ({

// })

const Regis = () => {
  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');
  const [notelp, setNotelp] = useState('');
  const [pw, setPw] = useState('');

  const dispatch = useDispatch();
  const register = (satu, dua, tiga, empat) => {
    dispatch(SignUp(satu, dua, tiga, empat));
  };
  console.log(nama);
  console.log(alamat);
  console.log(notelp);
  console.log(pw);

  return (
    <View style={styles.container}>
      <View style={{alignSelf: 'center'}}>
        <Image source={Logo2} style={styles.image} />
      </View>
      <View style={styles.top1}>
        <View style={styles.top2}>
          <Text style={styles.text}>Hi !</Text>
          <Text style={styles.text2}>Create a new account</Text>
        </View>
        <View style={styles.top3}>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn0.iconfinder.com/data/icons/printing-color-outline/64/8-name_card-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              // Label={'Nama'}
              style={styles.data}
              placeholder={'Masukan Nama'}
              values={nama}
              onChangeTexts={text => {
                setNama(text);
              }}
              // TypeKey={'number-pad'}
            />
          </View>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn4.iconfinder.com/data/icons/communication-media-color-pop-vol-2/64/phone-call-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              // Label={'Nama'}
              style={styles.data}
              placeholder={'Masukan No Telp'}
              values={notelp}
              onChangeTexts={text => {
                setNotelp(text);
              }}
              // TypeKey={'number-pad'}
            />
          </View>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn3.iconfinder.com/data/icons/strokeline/128/02_icons-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              // Label={'Nama'}
              style={styles.data}
              placeholder={'Masukan alamat'}
              values={alamat}
              onChangeTexts={text => {
                setAlamat(text);
              }}
              // TypeKey={'number-pad'}
            />
          </View>
          <View style={styles.top4}>
            <Image
              source={{
                uri: 'https://cdn4.iconfinder.com/data/icons/protection-and-security-12/64/14-Password-512.png',
              }}
              style={styles.icon}
            />
            <TextInput
              Label={'Nama'}
              style={styles.data}
              placeholder={'Masukan pass'}
              values={pw}
              secureTextEntry={true}
              onChangeTexts={text => {
                setPw(text);
              }}
              // TypeKey={'number-pad'}
            />
          </View>
        </View>
      </View>
      <TouchableOpacity
        style={styles.regis}
        onPress={() => {
          Alert.alert('Hi New User!!! ');
          register(nama, notelp, alamat, pw);
        }}>
        <Text style={styles.textregis}>SIGN UP</Text>
      </TouchableOpacity>
      {/* <View style={{paddingTop: 10, paddingHorizontal: 160}}>
        <Button
          title="SUBMIT"
          onPress={() => {
            register(nama, notelp, alamat, pw);
          }}
        />
      </View> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F0FFFF',
    flexDirection: 'column',
  },
  top1: {
    alignItems: 'center',
    flexDirection: 'column',
  },
  top2: {
    alignSelf: 'center',
    paddingHorizontal: 35,
    paddingVertical: 5,
  },
  top3: {
    alignSelf: 'center',
    paddingHorizontal: 35,
  },
  top4: {
    flexDirection: 'row',
    borderBottomWidth: 1,
  },
  data: {
    fontSize: 20,
    width: 300,
    padding: 15,
    borderBottomColor: 'black',
  },
  regis: {
    fontSize: 50,
    backgroundColor: '#00BFFF',
    marginTop: 20,
    width: 150,
    padding: 10,
    borderRadius: 15,
    alignSelf: 'center',
  },
  image: {
    // flex: 1,
    height: 200,
    width: 400,
    resizeMode: 'contain',
    backgroundColor: '#F0FFFF',
    // alignItems: 'stretch',
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 28,
    color: 'black',
    textAlign: 'center',
  },
  text2: {
    fontFamily: 'Montserrat-Light',
    fontSize: 20,
    color: 'black',
    textAlign: 'center',
  },
  textregis: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
  },
  icon: {
    height: 30,
    width: 30,
    marginTop: 20,
  },
});

export default Regis;