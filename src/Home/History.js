History.js

import {View, Text, StyleSheet, style, ScrollView, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {History} from '../Redux/Action/Action';
import Logo1 from '../../Assets/history2.png';

const DataHistory = ({navigation}) => {
  const [databaru, setDatabaru] = useState();
  const dispatch = useDispatch();
  const state = useSelector(state => state.fetch);
  let arr = Object.entries(state.History);
  async function historydata() {
    dispatch(History());
  }
  useEffect(() => {
    historydata();
  }, []);

  return (
    <View style={styles.container}>
      <Image source={Logo1} style={styles.image} />
      <Text style={styles.text}>Recent Transaction</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
          {arr.map((e, index) => {
            return (
              <View style={styles.list} key={index}>
                <Text style={styles.No}>NO : {index}</Text>
                <Text style={styles.data}>ID : {e[0]}</Text>
                <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                <Text style={styles.data}>Type : {e[1].type}</Text>
                <Text style={styles.data}>Biaya : {e[1].amount}</Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lavenderblush',
  },
  list: {
    marginVertical: 10,
    backgroundColor: 'white',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: 'white',
    width: 300,
  },
  body: {
    alignItems: 'center',
    flex: 3,
    backgroundColor: 'white',
    borderRadius: 50,
    borderColor: '#ADD8E6',
    borderWidth: 2,
    marginHorizontal: 20,
  },
  No: {
    fontWeight: 'bold',
    color: 'black',
    fontFamily: 'Montserrat-Light',
  },
  data: {
    fontWeight: '600',
    color: 'black',
    fontFamily: 'Montserrat-Regular',
  },
  image: {
    // flex: 1,
    height: 200,
    width: 400,
    resizeMode: 'center',

    // alignItems: 'stretch',
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
    color: 'black',
    marginHorizontal: 25,
    paddingBottom: 10,
  },
});

export default DataHistory;

