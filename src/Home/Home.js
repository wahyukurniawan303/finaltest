import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Image,
  Alert
} from 'react-native';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {DEL} from '../Redux/Action/Action';
import Logo1 from '../../Assets/transfer.png';
import Logo2 from '../../Assets/pulsa.png';
import Logo3 from '../../Assets/history.png';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const Log = async () => {
    try {
      dispatch(DEL());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log(e, 'error');
    }
  };


  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.text}>WelcomeBack</Text>
        <View style={styles.top}>
          <Image
            source={{
              uri: 'https://cdn4.iconfinder.com/data/icons/buno-info-signs/32/__search_magnifier_explore-512.png',
            }}
            style={styles.icon2}
          />
        </View>
      </View>
      <View style={styles.middle} />
      <View style={{padding: 10, alignSelf: 'flex-start'}}>
        <Text style={styles.text2}>Get Your Choice!</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'stretch',
          paddingVertical: 20,
        }}>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Transfer');
            }}>
            <View style={styles.bottom}>
              <Image source={Logo1} style={styles.image} />
            </View>
          </TouchableOpacity>
          <Text>Transfer</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Pulsa');
            }}>
            <View style={styles.bottom}>
              <Image source={Logo2} style={styles.image} />
            </View>
          </TouchableOpacity>
          <Text>Pulsa</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('History');
            }}>
            <View style={styles.bottom}>
              <Image source={Logo3} style={styles.image} />
            </View>
          </TouchableOpacity>
          <Text>History</Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            onPress={async () => {
              Alert.alert('Thanks For Use This App ');
              Log();
            }}>
            <View style={styles.bottom}>
              <Image
                source={{
                  uri: 'https://cdn2.iconfinder.com/data/icons/user-interface-elements-2/60/door-out-512.png',
                }}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
          <Text>Sign out</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: 'stretch',
    backgroundColor: 'lavenderblush',
    flexDirection: 'column',
    alignItems: 'stretch',
    padding: 30,
  },
  top: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 20,
    height: 30,
    width: 100,
    alignSelf: 'stretch',
    marginLeft: 90,
    marginTop: 7,
  },
  middle: {
    // flex: 0.3,
    backgroundColor: 'darkturquoise',
    height: 150,
    borderRadius: 20,
    width: 350,
    marginTop: 20,
    elevation: 10,
    shadowColor: 'blue',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  bottom: {
    height: 50,
    width: 50,
    borderRadius: 10,
    backgroundColor: 'darkturquoise',
    // borderWidth: 1,
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    elevation: 20,
    shadowColor: '#104ea1',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  text: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 28,
    color: 'black',
  },
  text2: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 20,
    color: 'black',
  },
  image: {
    width: 100,
    height: 100,
  },
  icon: {
    width: 50,
    height: 50,
  },
  icon2: {
    width: 25,
    height: 25,
  },
});
export default Home;