import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Image,
  Alert,
} from 'react-native';
import React from 'react';
import Axios from 'axios';
import {useState} from 'react';

import {History, Transaksi} from '../Redux/Action/Action';
import {useDispatch, useSelector} from 'react-redux';
import Logo1 from '../../Assets/transfer.png';

const Transfer = ({navigation}) => {
  const [amount, setAmount] = useState();
  const [sender, setSender] = useState('NMalhNh5LPYMCq7uuUh');
  const [target, setTarget] = useState('-NMalhNh5LPYMCq7uuUh');
  const [transaksi, setTransaksi] = useState('Transfer');
  const dispatch = useDispatch();
  const state = useSelector(state => state.fetch);
  async function transaksidata(am, sen, tar, ty) {
    dispatch(Transaksi(am, sen, tar, ty));
  }

  const Rupiah = x => {
    if (x) {
      return x
        .toString()
        .replace(/\./g, '')
        .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
    }
  };
  const Bongkar = async () => {
    let hapusTitik = nominal.toString().split('.').join('');
    let finish = Number(hapusTitik);
    await HitApi(finish);
  };
  return (
    <View style={[styles.container]}>
      <View style={{alignSelf: 'center'}}>
        <Image source={Logo1} style={styles.image} />
      </View>
      <Text style={styles.text}>Tuku Pulsa</Text>
      <View style={styles.top}>
        <TextInput
          placeholder="Minimal 20.000"
          value={amount}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setAmount(Rupiah(text || 0));
          }}
        />
      </View>

      <View style={styles.bottom}>
        <TextInput
          placeholder="Masukkan Nominal"
          value={target}
          onChangeText={text => {
            setTarget(text);
          }}
        />
      </View>
      <View style={styles.send}>
        <TouchableOpacity
        onPress={async () => {
            let coba = amount.toString().split('.').join('');
            console.log(coba, 'berhasil');
            const Numbers = Number(coba);
            if (Numbers >= 50000) {
              Alert.alert('Nominal kamu tidak cukup');
            } else {
              await transaksidata(amount, sender, target, transaksi).then(
                () => {
                  setAmount('');
                  Alert.alert(`berhasil Beli Pulsa \n dengan nominal ${amount}`);
                },
              );
            }
          }}>

          <Text style={styles.text2}>Buy</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lavenderblush',
    alignItems: 'center',
  },
  image: {
    // flex: 1,
    height: 200,
    width: 400,
    resizeMode: 'center',
  },
  top: {
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 30,
    width: 350,
    borderColor: '#ADD8E6',
    marginVertical: 20,
    backgroundColor: 'white',
  },
  bottom: {
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 30,
    width: 350,
    borderColor: '#ADD8E6',

    backgroundColor: 'white',
  },
  text: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
    color: 'black',
    marginHorizontal: 25,
    paddingBottom: 10,
  },
  text2: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
  },
  send: {
    fontSize: 50,
    backgroundColor: 'honeydew',
    marginTop: 30,
    width: 150,
    padding: 10,
    borderRadius: 15,
    alignSelf: 'center',
  },
});

export default Transfer;
