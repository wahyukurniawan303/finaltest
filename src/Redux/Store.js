import {createStore, applyMiddleware} from 'redux';
import rootReducer from './Reducer/Index';
import thunk from 'redux-thunk';

// 1. kita membuat sebuah store di mana akan menyimpan data sebagai middleware

const storeRedux = createStore(rootReducer, applyMiddleware(thunk));

//2 kalau mau pake saga sagaMiddleware.run(rootSaga);

export default storeRedux;