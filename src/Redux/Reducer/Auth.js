const {act} = require('react-test-renderer');

const initialState = {
  isLoading: false,
  AuthData: null,
};
const Auth = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isLoading: true,
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoading: true,
        AuthData: action.data,
      };
    case 'REGISTER_SUCCESS':
      return {
        ...state,
        isLoading: true,
        AuthData: action.data,
      };
    case 'SIGN_OUT':
      return {
        ...state,
        isLoading: true,
        AuthData: action.data,
      };
    default:
      return state;
  }
};
export default Auth;