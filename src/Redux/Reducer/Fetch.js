const initialState = {
  isLoading: false,
  Fetchdata: [],
  History: [],
  Transaksi: [],
};
const Fetch = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DATA':
      return {
        ...state,
        loading: true,
      };
    case 'GET_HISTORY':
      return {
        ...state,
        loading: false,
        History: action.data,
      };
    case 'GET_TRANSAKSI':
      return {
        ...state,
        loading: false,
        Transaksi: action.data,
      };
    default:
      return state;
  }
};

export default Fetch;