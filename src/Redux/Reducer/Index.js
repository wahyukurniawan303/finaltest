import {combineReducers} from 'redux';
import auth from './Auth';
import fetch from './Fetch';

export default combineReducers({
  auth,
  fetch,
});