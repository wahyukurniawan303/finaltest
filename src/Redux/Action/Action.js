import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Getone = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      name: 'joda',
      phone: '083812838123',
      address: 'test',
      password: '123',
    });

    var config = {
      method: 'get',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NNApVhOW0Vk-WPIikYj.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'LOGIN_SUCCESS',
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const SignUp = (nama, telp, alamat, pw) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      name: nama,
      phone: telp,
      address: alamat,
      password: pw,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGISTER_SUCCESS',
          data: data,
        });
        dataToken(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const History = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'GET_HISTORY',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Transaksi = (am, send, tar, ty) => {
  return async dispatch => {
    var data = JSON.stringify({
      amount: am,
      sender: send,
      target: tar,
      type: ty,
    });

    var config = {
      method: 'post',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'content-type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        const data = JSON.stringify(response.data);
        console.log(data, 'berhasil');
        dispatch({
          type: 'GET_TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
    return true;
  };
};

export const DEL = () => {
  const data = '';
  return async dispatch => {
    dispatch({
      type: 'SIGN_OUT',
      data: data,
    });
  };
};